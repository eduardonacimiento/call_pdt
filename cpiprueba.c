#include "mpi.h"
#include <stdio.h>
#include <math.h>

// CALL DEFS
#pragma cll parallel MPI
#pragma cll uses PAPI

#define MAX_NUM_INTERVALS 10000000
double f( double );
double f( double a )
{
    return (4.0 / (1.0 + a*a));
}

int main( int argc, char *argv[]) {
    int n, myid, numprocs, i;
    double PI25DT = 3.141592653589793238462643;
    double mypi, pi, h, sum, x;

    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD,&myid);
    
#pragma cll sync ker PAPI_REAL_USEC,PAPI_L2_TCM=ker[0]
    n = 100000000;
    h   = 1.0 / (double) n;
    sum = 0.0;
    for (i = myid + 1; i <= n; i += numprocs)
    {
        x = h * ((double)i - 0.5);
        sum += f(x);
    }
    mypi = h * sum;

    printf ("Working at process ID: %d\n", myid);

    MPI_Reduce(&mypi, &pi, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
#pragma cll end ker
  

    if (myid == 0)
    {
        printf("pi is approximately %.16f, Error is %.16f\n",
               pi, fabs(pi - PI25DT));
    }

#pragma cll report ker
    MPI_Finalize();

    return 0;
}

            
