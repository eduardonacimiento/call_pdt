#!/bin/sh
##
## Nombre del trabajo que se va a crear
##
#PBS -N cll_ex
##
## Cola donde se lanzará el trabajo
##
#PBS -q batch
##
## Número de procesadores que se van a utilizar. En este caso se está solicitando 
## la ejecución en 2 nodos con 1 procesos por nodo.
##
#PBS -l nodes=2:ppn=1
##
## Tiempo máximo de ejecución del trabajo. El formato es HH:MM:SS.
##
#PBS -l walltime=00:10:00
##
## Se van a volcar a fichero tanto la salida estándar como la salida de errores
##
#PBS -k oe

# Se ha de indicar la ruta absoluta al programa a ejecutar
export  LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/soft/papi/lib
#mpirun -np 2 /opt/soft/CALL/example/example_cll.exe
mpirun -np 2 /home/bejeque/eduardo/call_pdt/cpiprueba_cll.exe
