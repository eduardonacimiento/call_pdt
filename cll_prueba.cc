/*************************************************************************/
/* DUCTAPE Version 2.0                                                   */
/* Copyright (C) 2001                                                    */
/* Forschungszentrum Juelich, Zentralinstitut fuer Angewandte Mathematik */
/* University of Oregon, Department of Computer and Information Science  */
/*************************************************************************/

# include <fstream>
# include <algorithm>
# include <vector>
# include <string>

extern void pdb_ERROR(const char *msg, const char *item, int val);
#include <unistd.h>
#include "pdbAll.h"
#include "pdbTraits.h"
#include "pdbTdefs.h"

using namespace std;

//---------------------------------------------------------------------------
  int pragma_id = 0;
  vector<string> directives;

  /*
   * find_c_next_word:  find next word in preprocessor statement
   *                  IN:  preStmt   :  lines of preprocessor statement 
   *                  IN:  size      :  number of lines
   *                  IN:  pline, pos:  start location for search
   *                 OUT:  pline, pos:  start location for next statement
   *                 OUT:  wbeg      :  start position of word
   */
  string find_c_next_word(vector<string>& preStmt, unsigned size, unsigned& pline, string::size_type& ppos, string::size_type& wbeg) {
    while ( pline < size ) {
      wbeg = preStmt[pline].find_first_not_of(" \t", ppos);
      if ( preStmt[pline][wbeg] == '\\' || wbeg == string::npos ) {
        ++pline;
        if ( pline < size ) { ppos = 0; } else { return ""; }
      } 
      else {
        ppos = preStmt[pline].find_first_of(" \t()", wbeg);
        return preStmt[pline].substr(wbeg, ppos==string::npos ? ppos : ppos-wbeg);
      }
    }
    return "";
  }


  /*
   * find_c_kind:  find next word in preprocessor statement
   *             IN:  preStmt    :  lines of preprocessor statement 
   *             IN:  size       :  number of lines
   *             IN:  name       :  name of pragma
   *             IN:  kline, kpos:  start location for search
   */
  string find_c_kind(vector<string>& preStmt, unsigned size, const string& name, unsigned kline, string::size_type kpos) {
    string::size_type kbeg = 0;
    string kind = find_c_next_word(preStmt, size, kline, kpos, kbeg);
    return kind;
  }

  /*
   * process_preStmt:  determine if preprocessor statement is pragma
   *              IN:  preStmt     :  lines of preprocessor statement 
   *              IN:  lineno, ppos:  location of '#'
   *              IN:          file:  file pointer
   *              IN:           pdb:  program database
   */
  bool process_preStmt(vector<string>& preStmt, int lineno, string::size_type ppos, pdbFile* file, PDB& pdb) {
    unsigned s = preStmt.size();
    bool inComment = false;

    // pragma?
    unsigned nline = 0;
    string::size_type npos = ppos;
    string::size_type nbeg = 0;
    if ( find_c_next_word(preStmt, s, nline, npos, nbeg) == "pragma" ) {
      // determine name (= next word after pragma)
      string name = find_c_next_word(preStmt, s, nline, npos, nbeg);

      // determine kind (= next word after name)
      string kind = find_c_kind(preStmt, s, name, nline, npos);

      // generate pragma text the PDB way
      string ptext;
      for (unsigned i=0; i<s; ++i) {
	if ( i )
          ptext += preStmt[i];
	else
          ptext += preStmt[i].substr(ppos-1);
        if ( i != (s-1) ) ptext += "n";
      }

      cout << "PRAGMA DETECTADO\n";
      pdbPragma* pr = pdb.findItem(PDB::pragmaTag(), name, ++pragma_id);
      pr->kind(kind);
      pr->location(pdbLoc(file, lineno+nline, nbeg+1));
      pr->prBegin(pdbLoc(file, lineno, ppos));
      pr->prEnd(pdbLoc(file, lineno+s-1, preStmt[s-1].size()));
      pr->text(ptext);
      cout << "\tNombre:\t" << pr->name() << endl;
      cout << "\tTipo:\t" << pr->kind() << endl;
      cout << "\tLugar:\t" << pr->location() << endl;
      cout << "\tInicio:\t" << pr->prBegin() << endl;
      cout << "\tFin:\t" << pr->prEnd() << endl;
      cout << "\tTexto:\t" << pr->text() << endl;

    }
    return inComment;
  }


  /*
   * find_c_comments_in:  find comments and pragmas in file 'name'
   *                    IN:  file:  file pointer
   *                    IN:  pdb :  program database
   */
  void find_c_comments_in(pdbFile* file, PDB& pdb) {
    const string& name = file->name();
    string line;
    bool inComment = false;
    bool preContLine = false;
    int lineno = 1;
    string::size_type lstart = string::npos;
    vector<string> preStmt;

    ifstream is(name.c_str());
    if ( !is ) {
      cerr << "ERROR: cannot open " << name << endl;
      return;
    }

    // CLL START
    std::string fn = name.c_str();
    fn.replace(fn.find_last_of("."), 1, ".cll.");
    ofstream os_cll(fn.c_str());
    if ( !os_cll ) {
      cerr << "ERROR: cannot open " << fn << endl;
      return;
    }
    // CLL END

    while ( getline(is, line) ) {
      if (((lstart = line.find_first_not_of(" \t")) != string::npos) && line[lstart] == '#' ) {
        /*
         * preprocessor directive
         */
        preStmt.push_back(line);
        if ( line[line.size()-1] == '\\' ) {
          preContLine = true;
        } else {
          inComment = process_preStmt(preStmt, lineno, lstart+1, file, pdb);
          preStmt.clear();
        }
  /*  	// REVISAR FALTAN LOS #includes
	unsigned s = preStmt.size();
    	string::size_type npos = lstart+1;
    	string::size_type nbeg = 0;
	if (find_c_next_word(preStmt, s, lineno, npos, nbeg) != "pragma") {
          os_cll << line;
          cout << line;
	}*/

	cout << line << endl; 
      }
      else {
	cout << line << endl; 
	os_cll << line << endl;
      }
      ++lineno;
    }
  }

//---------------------------------------------------------------------------

int main(int argc, char *argv[]) {
  directives.push_back("$omp");

  // open and initialize program database
  PDB pdb(argv[optind]);
  if ( !pdb ) return 1;

  // build file list
  PDB::filevec& files = pdb.getFileVec();

  for(PDB::filevec::iterator fi=files.begin(); fi!=files.end(); ++fi) {
    if ( ! (*fi)->isSystemFile() ) {
      if ( pdb.language() & PDB::LA_C_or_CXX ) {
        find_c_comments_in(*fi, pdb);
      }
    } else {
    }
  }

  pdb.finalCheck(PDB::pragmaTag());

  //pdb.write(cout);

  return 0;
}
