/*************************************************************************/
/* DUCTAPE Version 2.0                                                   */
/* Copyright (C) 2001                                                    */
/* Forschungszentrum Juelich, Zentralinstitut fuer Angewandte Mathematik */
/* University of Oregon, Department of Computer and Information Science  */
/*************************************************************************/
/* DUCTAPE_CLL                                                           */
/* Modified by Eduardo Nacimiento García, 2016                           */
/* University of La Laguna, Canary Islands                               */
/*************************************************************************/



#ifdef _OLD_HEADER_
# include <fstream.h>
# include <algo.h>
# include <vector.h>
#else
# include <fstream>
  using std::ifstream;
  using std::ofstream;
# include <algorithm>
  using std::transform;
#include <vector>
  using std::vector;
# include <string>
  using std::getline;
#endif

extern void pdb_ERROR(const char *msg, const char *item, int val);
#ifdef _MSC_VER
 #include "getopt.h"
#else
 #include <unistd.h>
#endif
#include <ctype.h>
#include <cmath>
#include "pdbAll.h"
#include "pdbTraits.h"
#include "pdbTdefs.h"

//---------------------------------------------------------------------------
namespace {
  int pragma_id = 0;
  int comment_id = 0;
  bool gen_comments = true;
  bool gen_pragmas = true;
  vector<string> directives;
  char dComChar = 'c';

  struct fo_tolower : public std::unary_function<int,int> {
    int operator()(int x) const {
      return tolower(x);
    }
  };
  
  
   int default_uses = 0;
   int line_cll_c = 0;
  
   #define ONE(x) (((x)==0)?1:x)
  

  
  // Inicializando valores del experimento
  int nobs = 0;
  struct sp_struct {
     int maxtests;
     int dimension;
     int numidents;
     int begin_lines;
     int end_lines;
     std::string name;
     std::string par;
     std::string formula;
     std::string informula;
     int component;
     int poscomponent;
  };
  
  sp_struct sp = {131072,0,0,0,0,"","","","",0,0};

   vector<string> vec_uses;
   vector<string> vec_exp;


  vector<string> explode(const string& str, const char& ch) {
    string next;
    vector<string> result;
    for (string::const_iterator it = str.begin(); it != str.end(); it++) {
        if (*it == ch) {
            if (!next.empty()) {
                result.push_back(next);
                next.clear();
            }
        } else {
            next += *it;
        }
    }
    if (!next.empty())
         result.push_back(next);
    return result;
  }


 void cll_f_kind_uses(ofstream& os_cllf, pdbPragma *pr) {
    vector<string> vec_param;
    vec_param = explode(pr->text(), ' ');
    string uses_name = vec_param[2];
    vec_uses.push_back(uses_name);
  }



 void cll_c_kind_uses(ofstream& os_cllh, pdbPragma *pr) {
    vector<string> vec_param;
    vec_param = explode(pr->text(), ' ');
    string uses_name = vec_param[3];
    vec_uses.push_back(uses_name);
  }


  vector<string> cll_c_kind_sync(ofstream& os_cll, pdbPragma *pr) {
     //line_cll_c++;
     //os_cll << "CLL_SYNC;" << endl;
     vector<string> vec_param;
     vec_param = explode(pr->text(), ' ');
     string varname = vec_param[3];
     sp.name = varname;
     vector<string> vec_opt;
     vec_opt = explode(vec_param.back(), ',');
     for (int i = 0; i < 2; i++) {
       line_cll_c++;
       os_cll << endl;
       for(std::vector<string>::iterator it = vec_opt.begin(); it != vec_opt.end(); ++it) {
         string tmp = *it;
         vector<string> vec_tmp;
         vec_tmp = explode(*it, '=');
         sp.informula = vec_tmp.back();
         // NO SE ESTÁ USANDO EN CALL (INICIO)
         sp.formula = "p 0 ";
         sp.dimension = 1;
         sp.component = 1;
         sp.poscomponent = 1;
         // (FIN)
         
         if (i == 0) {
           vec_exp.push_back(vec_tmp.front());
           line_cll_c++;
           os_cll << "\t"  << vec_tmp.front() << "_INIT(" << varname << ");" << endl; 
         }
         else {
           line_cll_c++;
           os_cll << "\t"  << vec_tmp.front() << "_START_OBS(" << varname << ");" << endl; 
         }
       }
     }
     nobs = vec_exp.size();
     return vec_opt;
  }




  vector<string> cll_f_kind_sync(ofstream& os_cllf, pdbPragma *pr) {
     vector<string> vec_param;
     vec_param = explode(pr->text(), ' ');
     string varname = vec_param[2];
     sp.name = varname;
     vector<string> vec_opt;
     vec_opt = explode(vec_param.back(), ',');
     for (int i = 0; i < 2; i++) {
       for(std::vector<string>::iterator it = vec_opt.begin(); it != vec_opt.end(); ++it) {
         string tmp = *it;
         vector<string> vec_tmp;
         vec_tmp = explode(*it, '=');
         sp.informula = vec_tmp.back();
         // NO SE ESTÁ USANDO EN CALL (INICIO)
         sp.formula = "p 0 ";
         sp.dimension = 1;
         sp.component = 1;
         sp.poscomponent = 1;
         // (FIN)
         
         if (i == 0) {
           vec_exp.push_back(vec_tmp.front());
           line_cll_c++;
           os_cllf << "      call "  << vec_tmp.front() << "_INIT(" << varname << ")" << endl; 
         }
         else {
           line_cll_c++;
           os_cllf << "      call "  << vec_tmp.front() << "_START_OBS(" << varname << ")" << endl; 
         }
       }
     }
     nobs = vec_exp.size();
     return vec_opt;
  }



  void cll_f_kind_end(ofstream& os_cllf, pdbPragma *pr, vector<string> &vec_opt) {
     vector<string> vec_param;
     vec_param = explode(pr->text(), ' ');
     string varname = vec_param[2];
     for (int i = 0; i < 3; i++) {
      for (int j = vec_exp.size() - 1; j >= 0; j--) {
         line_cll_c++;
         if (i == 0) {
           os_cllf << "      call "  << vec_exp[j] << "_STOP_OBS(" << varname << ")" << endl; 
           if (j == 0) {
              os_cllf << endl;
           }
         }
         else if (i == 1) {
           if (j != vec_exp.size() - 1) {
              os_cllf << "      call "  << vec_exp[j] << "_SAVE_INSTANCE(" << varname << ", " << varname << ".numtests++" << ")" << endl; 
           }  
           else {
              os_cllf << "      call "  << vec_exp[j] << "_SAVE_INSTANCE(" << varname << ", " << varname << ".numtests" << ")" << endl; 
           }
           
           if (j == 0) {
              os_cllf << endl;
              line_cll_c++;
           }
         }
         else {
           os_cllf << "      call "  << vec_exp[j] << "_FINALIZE(" << varname << ")" << endl; 
         }
       }
     }
  }



  void cll_c_kind_end(ofstream& os_cll, pdbPragma *pr, vector<string> &vec_opt) {
     vector<string> vec_param;
     vec_param = explode(pr->text(), ' ');
     string varname = vec_param[3];
     for (int i = 0; i < 3; i++) {
      for (int j = vec_exp.size() - 1; j >= 0; j--) {
         line_cll_c++;
         if (i == 0) {
           os_cll << "\t"  << vec_exp[j] << "_STOP_OBS(" << varname << ");" << endl; 
           if (j == 0) {
              os_cll << endl;
              line_cll_c++;
           }
         }
         else if (i == 1) {
           if (j != vec_exp.size() - 1) {
              os_cll << "\t"  << vec_exp[j] << "_SAVE_INSTANCE(" << varname << ", " << varname << ".numtests++" << ");" << endl; 
           }  
           else {
              os_cll << "\t"  << vec_exp[j] << "_SAVE_INSTANCE(" << varname << ", " << varname << ".numtests" << ");" << endl; 
           }
           
           if (j == 0) {
              os_cll << endl;
              line_cll_c++;
           }
         }
         else {
           os_cll << "\t"  << vec_exp[j] << "_FINALIZE(" << varname << ");" << endl; 
         }
       }
     }
  }


  void cll_f_kind_report(ofstream& os_cllf, pdbPragma *pr, vector<string> &vec_opt) {
     vector<string> vec_param;
     vec_param = explode(pr->text(), ' ');
     string varname = vec_param[2];
     for (int i = 0; i < 2; i++) {
       line_cll_c++;
       if (i == 0) {
           os_cllf << "\n!DEC$ DEFINE CLL_USERDEF_TITLES()";
       }
       else {
           os_cllf << "\n!DEC$ DEFINE CLL_USERDEF_VALUES(instance)";
       }
       for (int j = 0; j < vec_exp.size(); j++) {
         line_cll_c++;
         if (i == 0) {
           os_cllf << " \\" << endl << "      call "  << vec_exp[j] << "_PRINT_TITLE(" << varname << ")"; 
         }
         else {
           os_cllf << " \\" << endl << "      call"  << vec_exp[j] << "_PRINT_VALUE(" << varname << ",(instance))"; 
         }
       }
     }
     line_cll_c++;
     os_cllf << "\n      call cll_report(" << varname << ")" << endl;
     line_cll_c++;
     os_cllf << "!DEC$ UNDEFINE CLL_USERDEF_TITLES" << endl;
     line_cll_c++;
     os_cllf << "!DEC$ UNDEFINE CLL_USERDEF_VALUES" << endl;
     line_cll_c++;
     os_cllf << "! <END> CALL inserted F source code <END> " << endl;
  }


  void cll_c_kind_report(ofstream& os_cll, pdbPragma *pr, vector<string> &vec_opt) {
     vector<string> vec_param;
     vec_param = explode(pr->text(), ' ');
     string varname = vec_param[3];
     for (int i = 0; i < 2; i++) {
       line_cll_c++;
       if (i == 0) {
           os_cll << "\n#define CLL_USERDEF_TITLES()";
       }
       else {
           os_cll << "\n#define CLL_USERDEF_VALUES(instance)";
       }
       for (int j = 0; j < vec_exp.size(); j++) {
         line_cll_c++;
         if (i == 0) {
           os_cll << " \\" << endl << "\t"  << vec_exp[j] << "_PRINT_TITLE(" << varname << ");"; 
         }
         else {
           os_cll << " \\" << endl << "\t"  << vec_exp[j] << "_PRINT_VALUE(" << varname << ",(instance));"; 
         }
       }
     }
     line_cll_c++;
     os_cll << "\ncll_report(" << varname << ");" << endl;
     line_cll_c++;
     os_cll << "#undef CLL_USERDEF_TITLES" << endl;
     line_cll_c++;
     os_cll << "#undef CLL_USERDEF_VALUES" << endl;
     line_cll_c++;
     os_cll << "/* <END> CALL inserted C source code <END> */" << endl;
  }



  // Insert code for each kind
  void insert_cll_c_code(ofstream& os_cll, ofstream& os_cllh, pdbPragma *pr, vector<string> &vec_opt, int lineno, std::string name, std::string fn) {
    if (pr->kind() == "parallel") {
          vector<string> vec_param;
          vec_param = explode(pr->text(), ' ');
          sp.par = vec_param[3];
    }
    else if (pr->kind() == "uses") {
          cll_c_kind_uses(os_cllh, pr);
    }
    else if (pr->kind() == "sync") {
         line_cll_c += 3;
         os_cll << "CLL_SYNC;" << endl;
         os_cll << "#line " << line_cll_c << " \"" << fn << "\"" << endl;
         sp.begin_lines = lineno;
         vec_opt = cll_c_kind_sync(os_cll, pr);
         os_cll << "#line " << lineno + 1 << " \"" << name << "\"" << endl;
    }
    else if (pr->kind() == "end") {
         line_cll_c += 2;
         os_cll << "#line " << line_cll_c << " \"" << fn << "\"" << endl;
         sp.end_lines = lineno;
         cll_c_kind_end(os_cll, pr, vec_opt);
         os_cll << "#line " << lineno + 1 << " \"" << name << "\"" << endl;
    }
    else if (pr->kind() == "report") {
         line_cll_c += 2;
         os_cll << "#line " << line_cll_c << " \"" << fn << "\"" << endl;
         cll_c_kind_report(os_cll, pr, vec_opt);
         os_cll << "#line " << lineno + 1 << " \"" << name << "\"" << endl;
    }
    else {
       cout << "ERROR incorrect CLL pragma: " << pr->kind() << endl;
    }
  }
  
  // Inser code fpr each kind 
  void insert_cll_f_code(ofstream& os_cllf, pdbPragma *pr, vector<string> &vec_opt, int lineno, std::string name, std::string fn) {
    if (pr->kind() == "parallel") {
          vector<string> vec_param;
          vec_param = explode(pr->text(), ' ');
          sp.par = vec_param[2];
    }
    else if (pr->kind() == "uses") {
          cll_f_kind_uses(os_cllf, pr);
    }
    else if (pr->kind() == "sync") {
         line_cll_c += 3;
         os_cllf << "      call CLL_SYNC" << endl;
         os_cllf << "!line " << line_cll_c << " \"" << fn << "\"" << endl;
         sp.begin_lines = lineno;
         vec_opt = cll_f_kind_sync(os_cllf, pr);
         os_cllf << "!line " << lineno  << " \"" << name << "\"" << endl;
    }
    else if (pr->kind() == "end") {
         line_cll_c += 2;
         os_cllf << "!line " << line_cll_c << " \"" << fn << "\"" << endl;
         sp.end_lines = lineno;
         cll_f_kind_end(os_cllf, pr, vec_opt);
         os_cllf << "!line " << lineno  << " \"" << name << "\"" << endl;
    }
    else if (pr->kind() == "report") {
         line_cll_c += 2;
         os_cllf << "!line " << line_cll_c << " \"" << fn << "\"" << endl;
         cll_f_kind_report(os_cllf, pr, vec_opt);
         os_cllf << "!line " << lineno  << " \"" << name << "\"" << endl;
    }
    else {
       cout << "ERROR incorrect CLL directive: " << pr->kind() << endl;
    }
  }



  /*
   * find_[cf]_next_word:  find next word in preprocessor statement
   *                  IN:  preStmt   :  lines of preprocessor statement 
   *                  IN:  size      :  number of lines
   *                  IN:  pline, pos:  start location for search
   *                 OUT:  pline, pos:  start location for next statement
   *                 OUT:  wbeg      :  start position of word
   */
  string find_c_next_word(vector<string>& preStmt, unsigned size,
                          unsigned& pline, string::size_type& ppos,
                          string::size_type& wbeg) {
    while ( pline < size ) {
      wbeg = preStmt[pline].find_first_not_of(" \t", ppos);
      if ( preStmt[pline][wbeg] == '\\' || wbeg == string::npos ) {
        ++pline;
        if ( pline < size ) { ppos = 0; } else { return ""; }
      } else {
        ppos = preStmt[pline].find_first_of(" \t()", wbeg);
        return preStmt[pline].substr(wbeg,
                                     ppos==string::npos ? ppos : ppos-wbeg);
      }
    }
    return "";
  }

  string find_f_next_word(vector<string>& cStmt, unsigned size,
                          const string& sentinel,
                          unsigned& pline, string::size_type& ppos) {
    while ( pline < size ) {
      string::size_type wbeg = cStmt[pline].find_first_not_of(" \t", ppos);
      if ( cStmt[pline][wbeg] == '&' ) {
	ppos = wbeg+1;
      } else if ( wbeg == string::npos ) {
        ++pline;
        if ( pline < size ) {
	  ppos = cStmt[pline].find(sentinel) + sentinel.size() + 1;
        } else {
          return "";
        }
      } else if ( cStmt[pline][wbeg] == '(' || cStmt[pline][wbeg] == ')' ) {
        ppos = wbeg+1;
        return string(1, cStmt[pline][wbeg]);
      } else {
        ppos = cStmt[pline].find_first_of(" \t()", wbeg);
        return cStmt[pline].substr(wbeg, ppos==string::npos ? ppos : ppos-wbeg);
      }
    }
    return "";
  }

  /*
   * find_[cf]_kind:  find next word in preprocessor statement
   *             IN:  preStmt    :  lines of preprocessor statement 
   *             IN:  size       :  number of lines
   *             IN:  name       :  name of pragma
   *             IN:  kline, kpos:  start location for search
   */
  string find_c_kind(vector<string>& preStmt, unsigned size, const string& name,
                     unsigned kline, string::size_type kpos) {
    string::size_type kbeg = 0;
    string kind = find_c_next_word(preStmt, size, kline, kpos, kbeg);

    // special omp handling
    if ( name == "omp" ) {
      string w1 = kind, w2;

      if ( kind == "end" ) {
	w1 = find_c_next_word(preStmt, size, kline, kpos, kbeg);
	kind += w1;
      }
      if ( w1 == "parallel" ) {
	  string w2 = find_c_next_word(preStmt, size, kline, kpos, kbeg);
	  if ( w2 == "do" || w2 == "for" ||
	       w2 == "sections" || w2 == "workshare" ) kind += w2;
      }
    }
    return kind;
  }

  string find_f_kind(vector<string>& cStmt, unsigned size, const string& name,
                     unsigned kline, string::size_type kpos) {
    string kind = find_f_next_word(cStmt, size, name, kline, kpos);

    // special omp handling
    if ( name == "$omp" ) {
      string w1 = kind, w2;

      if ( kind == "end" ) {
	w1 = find_f_next_word(cStmt, size, name, kline, kpos);
	kind += w1;
      }
      if ( w1 == "parallel" ) {
	  string w2 = find_f_next_word(cStmt, size, name, kline, kpos);
	  if ( w2 == "do" || w2 == "for" ||
	       w2 == "sections" || w2 == "workshare" ) kind += w2;
      }
    }
    return kind;
  }

  /*
   * process_preStmt:  determine if preprocessor statement is pragma
   *              IN:  preStmt     :  lines of preprocessor statement 
   *              IN:  lineno, ppos:  location of '#'
   *              IN:          file:  file pointer
   *              IN:           pdb:  program database
   */
  bool process_preStmt(vector<string>& preStmt, int lineno,
                       string::size_type ppos, pdbFile* file, PDB& pdb, ofstream& os_cll, ofstream& os_cllh, bool &inCll, vector<string> &vec_opt) {
    unsigned s = preStmt.size();
    bool inComment = false;
    string ctext;
    int cli = 0, cco = 0;

    // remove comments
    for (unsigned i=0; i<s; ++i) {
      string::size_type pos = 0;
      string& line = preStmt[i];
      while ( pos < line.size() ) {
        if ( inComment ) {
          // look for comment end
          if ( line[pos] == '*' && line[pos+1] == '/' ) {
	    ctext += "*/";
            line[pos++] = ' ';

	    if ( gen_comments ) {
	      pdbComment* com = file->addComment(comment_id++);
	      com->kind(PDB::LA_C);
	      com->cmtBegin(pdbLoc(file, cli, cco));
	      com->cmtEnd(pdbLoc(file, lineno+i, pos+1));
	      com->text(ctext);
	    }
            ctext = "";
            inComment = false;
          } 

	  // otherwise copy but handle end of line correctly
	  if ( pos == (line.size()-1) ) {
            ctext += "\\n";
            line[pos++] = '\\';
	  } else {
	    ctext += line[pos];
            line[pos++] = ' ';
	  }
        } else if ( line[pos] == '/' ) {
          pos++;
          if ( line[pos] == '/' ) {
            // c++ comments
	    cli = lineno + i;
	    cco = pos;
	    ctext = line.substr(pos-1);
            line[pos-1] = ' ';
            line[pos++] = ' ';
            while ( pos < line.size() ) { line[pos++] = ' '; }

	    if ( gen_comments ) {
	      pdbComment* com = file->addComment(comment_id++);
	      com->kind(PDB::LA_CXX);
	      com->cmtBegin(pdbLoc(file, cli, cco));
	      com->cmtEnd(pdbLoc(file, lineno+i, pos));
	      com->text(ctext);
	    }
            ctext = "";
          } else if ( line[pos] == '*' ) {
            // c comment start
	    cli = lineno + i;
	    cco = pos;
	    ctext = "/*";
            line[pos-1] = ' ';
            line[pos++] = ' ';
            inComment = true;
          }
        } else {
          pos++;
        }
      }
    }

    // pragma?
    unsigned nline = 0;
    string::size_type npos = ppos;
    string::size_type nbeg = 0;
    if ( find_c_next_word(preStmt, s, nline, npos, nbeg) == "pragma" ) {
      // determine name (= next word after pragma)
      string name = find_c_next_word(preStmt, s, nline, npos, nbeg);

      // determine kind (= next word after name)
      string kind = find_c_kind(preStmt, s, name, nline, npos);

      // generate pragma text the PDB way
      string ptext;
      for (unsigned i=0; i<s; ++i) {
	if ( i )
          ptext += preStmt[i];
	else
          ptext += preStmt[i].substr(ppos-1);
        if ( i != (s-1) ) ptext += "n";
      }

      if ( gen_pragmas ) {
        pdbPragma* pr = pdb.findItem(PDB::pragmaTag(), name, ++pragma_id);
	     pr->kind(kind);
        pr->location(pdbLoc(file, lineno+nline, nbeg+1));
        pr->prBegin(pdbLoc(file, lineno, ppos));
        pr->prEnd(pdbLoc(file, lineno+s-1, preStmt[s-1].size()));
        pr->text(ptext);
	
	if (name == "cll") {
          std::string fn = file->name().c_str();
          fn.replace(fn.find_last_of("."), 1, ".cll.");
          //line_cll_c++;
          //os_cll << "#line " << line_cll_c << " \"" << fn << "\"" << endl; // #line XX
          
          insert_cll_c_code(os_cll, os_cllh, pr, vec_opt, lineno, file->name(), fn);
          inCll = true;
          //line_cll_c++;
          //os_cll << "#line " << lineno + 1 << " \"" << file->name() << "\"" << endl;
	}
      }
    }
    return inComment;
  }

  /*
   * get_fcom_prefix:  determine fortran comment prefix
   *              IN:  line:  comment line
   */
  string get_fcom_prefix(const string& line) {
    for (int i=0; i<directives.size(); ++i) {
      string& dir = directives[i];
#if (__GNUC__ == 2)
      if ( strncmp(line.c_str(), dir.c_str(), dir.size()) == 0) return dir; 
#else
      if ( line.compare(1, dir.size(), dir) == 0 ) return dir;
#endif /* GNU bug */
    }
    return line.substr(0,1);
  }

   /*
    * has_no_cont:  determine whether f90 directive has continuation line
    *               => last non-blank, non-comment character is '&'
    *          IN:  line:  comment line
    *          IN:   pos:  starting position for possible inline comment
    */
  bool has_no_cont(const string& line, int pos) {
    string::size_type com = line.find("!", pos);
    if ( com != string::npos ) --com;
    string::size_type amp = line.find_last_not_of(" \t", com);
    return ( line[amp] != '&' );
  }

  /*
   * process_fcom:  process fortran comment block
   *           IN:  cStmt      :  lines of fortran comment block
   *           IN:  sline, scol:  start location
   *           IN:         ecol:  end   location
   *           IN:         file:  file pointer
   *           IN:          pdb:  program database
   *           IN:       prefix:  comment prefix
   */
  void process_fcom(vector<string>& cStmt, int sline, int scol, int ecol,
                    pdbFile* file, PDB& pdb, const string& prefix, ofstream& os_cllf, bool& inCll, std::vector<string> &vec_opt) {
    printf ("prefix=%s\n", prefix.c_str());
    if ( prefix.size() > 1 ) {
      // directive/pragma
      if ( gen_pragmas ) {
        // process pragma text
        string ptext;
        unsigned s = cStmt.size();
        for (unsigned i=0; i<s; ++i) {
	  string& lowline = cStmt[i];
	  string line = lowline;
          transform(lowline.begin(), lowline.end(),
	            lowline.begin(), fo_tolower());

          string::size_type pos = string::npos;
	  pos = lowline.find(prefix) + prefix.size() + 1;
	  if ( (pos = lowline.find("!", pos)) != string::npos ) {
	    // inline comment; erase from pragma text
	    if ( gen_comments ) {
	      pdbComment* com = file->addComment(comment_id++);
	      com->kind(PDB::LA_FORTRAN);
	      com->cmtBegin(pdbLoc(file, sline+i, pos+1));
	      com->cmtEnd(pdbLoc(file, sline+i, cStmt[i].size()));
	      com->text(line.substr(pos));
	    }
	    lowline.erase(pos);
	  }
          ptext += lowline;
          if ( i != (s-1) ) ptext += "\\n";
        }

        // determine kind (= next word after name)
        string kind = find_f_kind(cStmt, s, prefix, 0, scol+prefix.size()+1);

	// generate pragma item pdb
        pdbPragma* pr;
        if (prefix == "$omp") {
            pr = pdb.findItem(PDB::pragmaTag(),
	                             prefix=="$omp"?"omp":prefix, ++pragma_id);
        }
        else if (prefix == "$cll") {
            pr = pdb.findItem(PDB::pragmaTag(),
	                             prefix=="$cll"?"cll":prefix, ++pragma_id);
        }
    	  pr->kind(kind);
        pr->location(pdbLoc(file, sline, scol+1));
        pr->prBegin(pdbLoc(file, sline, scol));
        pr->prEnd(pdbLoc(file, sline+s-1, ecol));
        pr->text(ptext);
        
        
        if (prefix == "$cll") {
           std::string fn = file->name().c_str();
           fn.replace(fn.find_last_of("."), 1, ".cll.");
           //os_cllf << "!line 1 \"" << fn << "\"" << endl; // #line XX
           
           insert_cll_f_code(os_cllf, pr, vec_opt, sline, file->name(), fn);
           inCll = true;
           //os_cllf << "!line " << sline << " \"" << file->name() << "\"" << endl;
        }
        
        
      }
    } else {
      // plain comment
      if ( gen_comments ) {
        // generate comment text the PDB way
        string ctext;
        unsigned s = cStmt.size();
        for (unsigned i=0; i<s; ++i) {
          ctext += cStmt[i];
          if ( i != (s-1) ) ctext += "\\n";
        }
	
	// generate comment entry
        pdbComment* com = file->addComment(comment_id++);
        com->kind(PDB::LA_FORTRAN);
        com->cmtBegin(pdbLoc(file, sline, scol));
        com->cmtEnd(pdbLoc(file, sline+s-1, ecol));
        com->text(ctext);
      }
    }
    cStmt.clear();
  }




  // Generate the cll.fi file
  void generate_cll_f (std::string name, std::string fn_h,  ofstream& os_cllf, vector<string> &vec_exp, vector<string> &vec_uses) {


      os_cllf << "!DEC$ IF DEFINED __CLL_HH__\n!DEC$ DEFINE __CLL_HH__" << endl;
      os_cllf << "\n!      GLOBAL OPTIONS \n" << endl;
      os_cllf << "!     the following will change to 1 when the first report has been done */" << endl;
      os_cllf << "      integer cll_first_report = 0;" << endl;
      os_cllf << "      character*255 cll_fdata = \"NULL\";" << endl;
         
      std::string opt_force_overwrite = "-2";
      std::string cll_output_name = "\"" + fn_h.substr(0, fn_h.find_last_of(".")) +  ".c.dat\"";
      std::string cll_VERSION = "1.0";
         
      os_cllf << "      integer cll_opt_overwrite = " << opt_force_overwrite << ";"<< endl;
      os_cllf << "      chararcter*255 cll_output_name = " << cll_output_name << ";"<< endl;
      os_cllf << "\n!     END OF GLOBAL OPTIONS */\n" << endl;
      os_cllf << "      include 'cll.h'" << endl;
      os_cllf << "      character*255 cll_programname = \"" <<  name.c_str() << "\";\n";
      os_cllf << "      character*10 cll_version = \"" << cll_VERSION << "\";\n" << endl;

      for(int i = 0; i < sp.dimension; i++)  {
         os_cllf << "      real function cll_" << sp.name << "_" << i << "(";
         os_cllf << ") \n         return " << sp.component  << "\n      end\n\n";
      }
         
      os_cllf << "!DEC$ DEFINE cll_arg_" << sp.name << " " << endl;
       
      for (int i = 0; i < sp.numidents; i++) {
         os_cllf << "      " << sp.name << ".instance[i].id[" << i << "]";
         if (i < sp.numidents - 1) {
            os_cllf << ",";
         }
      }
         
      os_cllf << "\n";

      vec_uses.push_back("UNISTD");
        
      for (int i = 0; i < vec_uses.size(); i++) {
         os_cllf << "      include 'measures/cll." <<vec_uses[i] << ".h'" << endl;
      }
         
      os_cllf << "\n      integer function cll_user_measurements_libs_init() \n";
        
      for (int i = 0; i < vec_uses.size(); i++) {
         os_cllf << "         " << vec_uses[i] << "_INITIALIZATION()\n";
      }
      os_cllf << "         cll_user_measurements_libs_init = CLL_OK\n";
      os_cllf << "         return\n      end\n\n";


      //os_cllf << "      program main" << endl << endl;
      os_cllf << "      type t_instance" << endl;
      os_cllf << "        real id;" << endl;
      for (int i = 0; i < vec_exp.size(); i++) {
         os_cllf << "        " << vec_exp[i] << "_DECL" << endl;
      }
      os_cllf << "      end type" << endl << endl;
      os_cllf << "      type cll_ker" << endl;
      os_cllf << "          character*255 name;" << endl;
      os_cllf << "          integer dimension; ! number of experiment constants " << endl;
      os_cllf << "          integer maxtests;" << endl;
      os_cllf << "          character*255 pardriver;" << endl;
      os_cllf << "          integer numidents;" << endl;
      os_cllf << "          integer numobservables;" << endl;
      os_cllf << "          integer numtests;" << endl;
      os_cllf << "          integer loopcount;" << endl;
      os_cllf << "          integer(" << ceil(log10(sp.begin_lines)) << ") begin_lines;" << endl;
      os_cllf << "          integer(" << ceil(log10(sp.end_lines)) << ") end_lines;" << endl;
      os_cllf << "          character*255 formula;" << endl;
      os_cllf << "          character*255 informula;" << endl;
      os_cllf << "          character(" << ONE(sp.numidents) << ")*255 idents;" << endl;
      os_cllf << "          character(" << ONE(nobs) << ")*255 observables;" << endl;
      os_cllf << "          character(" << sp.dimension << ")*255 components; ! functions for the design matrix (text) " << endl;
      os_cllf << "          character(" << sp.dimension << ")*255 poscomponents; ! postfix functions for the design matrix " << endl;
      os_cllf << "          real(" << sp.dimension << ") funccomp; ! functions for the design matrix" << endl;
      for (int i = 0; i < vec_exp.size(); i++) {
         os_cllf << "          " << vec_exp[i] << "_TMPS" << endl;
      }
      os_cllf << "          t_instance, dimension(131072) :: instance;" << endl;
      os_cllf << "        end type" << endl << endl;
      os_cllf << "        type(cll_" << sp.name << ") :: " << sp.name << endl;
      os_cllf << "        ker%name = \"" << sp.name << "\"" << endl;
      os_cllf << "        ker%dimension = " << sp.dimension << endl;
      os_cllf << "        ker%maxtests = " << sp.maxtests << endl;
      os_cllf << "        ker%pardriver = \"" << sp.par << "\"" << endl;
      os_cllf << "        ker%numidents = " << sp.numidents << endl;
      os_cllf << "        ker%numobservables = " << nobs << endl;
      os_cllf << "        ker%numtests = 0" << endl;
      os_cllf << "        ker%loopcount = 0" << endl;
      os_cllf << "        ker%begin_lines(1) = " << sp.begin_lines << endl;
      os_cllf << "        ker%end_lines(1) = " << sp.end_lines << endl;
      os_cllf << "        ker%formula = \"" << sp.formula << "\"" << endl;
      os_cllf << "        ker%informula = \"" << sp.informula << "\"" << endl;
      os_cllf << "        ker%idents(1) = \"\"" << endl;
      for (int i = 0; i < vec_exp.size(); i++) {
         os_cllf << "        ker%observables(" << i + 1 << ") = \""  << vec_exp[i] << "\"" << endl;
      }
      for (int i = 0; i < sp.dimension; i++) {
         os_cllf << "        ker%components(" << i + 1 << ") = \"" << sp.component << "\"" << endl;
      }
      for (int i = 0; i < sp.dimension; i++) {
         os_cllf << "        ker%poscomponents(" << i + 1 << ") = \"" << sp.poscomponent << "\"" << endl;
      }
      for (int i = 0; i < sp.dimension; i++) {
         os_cllf << "        ker%funccomp("  << i + 1 << ") = \"cll_" << sp.name << "_" << i <<"\"" << endl;
      }
    

      os_cllf << endl << endl << endl;

      os_cllf << "!DEC$ DEFINE __USE_" << sp.par << endl;
      os_cllf << "      include 'exps/cll_exp.h'" << endl;
      os_cllf << "      include 'comm/cll_" << sp.par << ".h'" << endl;
      os_cllf << "\n!DEC$ ENDIF ! __CLL_HH__ " << endl; 
     
  }

  // Generate the cll.h file
  void generate_cll_h (std::string name, std::string fn_h,  ofstream& os_cllh, vector<string> &vec_exp, vector<string> &vec_uses) {
     
         os_cllh << "#ifndef __CLL_HH__\n#define __CLL_HH__" << endl;
         os_cllh << "\n/* GLOBAL OPTIONS */\n" << endl;
         os_cllh << "#include <stdio.h> /* WE HAVE TO TRY TO AVOID THIS */ " << endl;
         os_cllh << "/* the following will change to 1 when the first report has been done */" << endl;
         os_cllh << "static char cll_first_report = 0;" << endl;
         os_cllh << "static FILE *cll_fdata = NULL;" << endl;
         
         std::string opt_force_overwrite = "-2";
         std::string cll_output_name = "\"" + fn_h.substr(0, fn_h.find_last_of(".")) +  ".c.dat\"";
         std::string cll_VERSION = "1.0";
         
         os_cllh << "static int cll_opt_overwrite = " << opt_force_overwrite << ";"<< endl;
         os_cllh << "static char *cll_output_name = " << cll_output_name << ";"<< endl;
         os_cllh << "\n/* END OF GLOBAL OPTIONS */\n" << endl;
         os_cllh << "#include \"cll.h\"" << endl;
         os_cllh << "char *cll_programname = \"" <<  name.c_str() << "\";\n";
         os_cllh << "char *cll_version = \"" << cll_VERSION << "\";\n" << endl;

         for(int i = 0; i < sp.dimension; i++)  {
            os_cllh << "double cll_" << sp.name << "_" << i << "(";
            os_cllh << ") {\n  return " << sp.component  << ";\n}\n\n";
         }
         
         os_cllh << "#define cll_arg_" << sp.name << " " << endl;
         
         for (int i = 0; i < sp.numidents; i++) {
            os_cllh << sp.name << ".instance[i].id[" << i << "]";
            if (i < sp.numidents - 1) {
               os_cllh << ",";
            }
         }
         
         os_cllh << "\n";

         //if (vec_uses.size() == 0) {
            vec_uses.push_back("UNISTD");
         //}
        
         for (int i = 0; i < vec_uses.size(); i++) {
            os_cllh << "#include <measures/cll." <<vec_uses[i] << ".h>" << endl;
         }
         
         os_cllh << "\nint cll_user_measurements_libs_init() {\n";
        
         for (int i = 0; i < vec_uses.size(); i++) {
            os_cllh << "\t" << vec_uses[i] << "_INITIALIZATION();\n";
         }
         
         os_cllh << "\treturn CLL_OK;\n}\n";

         os_cllh << "\nstruct cll_" << sp.name << " {" << endl;
         os_cllh << "  char *name;\n";
         os_cllh << "  unsigned dimension; /* number of experiment constants */\n";
         os_cllh << "  unsigned maxtests;\n";
         os_cllh << "  char *pardriver;\n";
         os_cllh << "  unsigned numidents;\n";
         os_cllh << "  unsigned numobservables;\n";
         os_cllh << "  unsigned numtests, loopcount;\n";
         os_cllh << "  unsigned begin_lines[" << ceil(log10(sp.begin_lines)) << "];\n";
         os_cllh << "  unsigned end_lines[" << ceil(log10(sp.end_lines)) << "];\n";
         os_cllh << "  char *formula;\n";
         os_cllh << "  char *informula;\n";
         os_cllh << "  char *idents[" << ONE(sp.numidents) << "];\n";
         os_cllh << "  char *observables[" << ONE(nobs) << "];\n";
         os_cllh << "  char *components[" << sp.dimension << "]; /* functions for the design matrix (text) */\n";
         os_cllh << "  char *poscomponents[" << sp.dimension << "]; /* postfix functions for the design matrix */\n";
         os_cllh << "  double (*funccomp[" << sp.dimension << "])(); /* functions for the design matrix */\n";
         
         for (int i = 0; i < vec_exp.size(); i++) {
               os_cllh << "  " << vec_exp[i] << "_TMPS;" << endl;
         }
          
         os_cllh << "  struct {\n";
         os_cllh << "    double id[" << ONE(sp.numidents) << "];\n";
         
         for(int i = 0; i < vec_exp.size(); i++) {
            os_cllh << "    " << vec_exp[i] << "_DECL;" << endl;
         }
         
         os_cllh << "  } instance[" << sp.maxtests << "];\n";
         os_cllh << "} " << sp.name << " = {\n";
         os_cllh << "  \"" << sp.name << "\",\n"; 
         os_cllh << "  " << sp.dimension << ",\n";
         os_cllh << "  " << sp.maxtests << ",\n";
         os_cllh << "  \"" << sp.par << "\",\n"; 
         os_cllh << "  " << sp.numidents << ",\n";
         os_cllh << "  " << nobs << ",\n";
         os_cllh << "  0, 0,\n";
         os_cllh <<"{ " << sp.begin_lines << "   },\n";
         os_cllh <<"{ " << sp.end_lines << "   },\n";
         os_cllh << "  \"" << sp.formula << "\",\n";
         os_cllh << "  \"" << sp.informula << "\",\n";
         
         /* print variables associated with the experiment */
         os_cllh << "  { ";
         os_cllh << "\"\"";
         os_cllh << "},\n";
         os_cllh << "  { ";
         
         for (int i = 0; i < vec_exp.size(); i++) {
            os_cllh << "\"" << vec_exp[i] << "\", ";
         }
         
         os_cllh << "},\n";
         os_cllh << "  { ";
         
         for(int i = 0; i < sp.dimension; i++) {
            os_cllh << "\"" << sp.component << "\", ";
         }   
         
         os_cllh << " },\n";
         os_cllh << "  { ";
         
         for(int i = 0; i < sp.dimension; i++) {
            os_cllh << "\"" <<  sp.poscomponent  << "\", ";
         }   
         
         os_cllh << " },\n";  
         os_cllh << "  { ";
         
         for(int i = 0; i < sp.dimension; i++) {
            os_cllh << "cll_" << sp.name << "_" << i << ", ";
         }
         
         os_cllh << " },\n";
         os_cllh << "};\n";
         os_cllh << "#define __USE_" << sp.par << endl;
         os_cllh << "#include \"exps/cll_exp.h\"" << endl;
         os_cllh << "#include \"comm/cll_" << sp.par << ".h\"" << endl;
         os_cllh << "\n#endif /* __CLL_HH__ */" << endl;

  }






  /*
   * find_[fc]_comments_in:  find comments and pragmas in file 'name'
   *                    IN:  file:  file pointer
   *                    IN:  pdb :  program database
   */
  void find_c_comments_in(pdbFile* file, PDB& pdb, vector<string> &vec_opt) {
    const string& name = file->name();
    string line;
    bool inComment = false;
    bool inCll = false;
    bool preContLine = false;
    int lineno = 1;
    string::size_type pos = 0;
    string::size_type lstart = string::npos;
    vector<string> preStmt;
    string ctext;
    int cli = 0, cco = 0;

    ifstream is(name.c_str());
    if ( !is ) {
      cerr << "ERROR: cannot open " << name << endl;
      return;
    }

    // CLL START
    std::string fn = name.c_str();
    fn.replace(fn.find_last_of("."), 1, ".cll.");
    ofstream os_cll(fn.c_str());
    if ( !os_cll ) {
      cerr << "ERROR: cannot open " << fn << endl;
      return;
    }
    
    std::string fn_h = name.c_str();
    std::string fn_cllh = fn_h.substr(0, fn_h.find_last_of("."))  + ".cll.h";
    ofstream os_cllh(fn_cllh.c_str());
    if ( !os_cllh ) {
      cerr << "ERROR: cannot open " << fn << endl;
      return;
    }

    //os_cll << "#line 1 \"" << fn << "\"" << endl;
    line_cll_c++;
    os_cll << "#include \"" << fn_h.substr(0, fn_h.find_last_of(".")) << ".cll.h\"" << endl;
    line_cll_c++;
    os_cll << "#line " << lineno << " \"" << name << "\"" << endl;
    

    int pos_main = 0;
    for (PDB::croutinevec::iterator r = pdb.getCRoutineVec().begin(); r!=pdb.getCRoutineVec().end(); r++) {
      if ((*r)->name() == "main") {
        pos_main = (*r)->location().line();
        break;
      }
    }
    
    // CLL END

    comment_id = 0;

    while ( getline(is, line) ) {
      if ( preContLine ) {
        /*
         * preprocessor directive continuation
         */
        preStmt.push_back(line);
        if ( line[line.size()-1] != '\\' ) {
          preContLine = false;
          inComment = process_preStmt(preStmt, lineno-preStmt.size()+1,
	                              lstart+1, file, pdb, os_cll, os_cllh, inCll, vec_opt);
          preStmt.clear();
        }

      } else if ( !inComment && 
           ((lstart = line.find_first_not_of(" \t")) != string::npos) &&
           line[lstart] == '#' ) {
        /*
         * preprocessor directive
         */
        preStmt.push_back(line);
        if ( line[line.size()-1] == '\\' ) {
          preContLine = true;
        } else {
          inComment = process_preStmt(preStmt, lineno, lstart+1, file, pdb, os_cll, os_cllh, inCll, vec_opt);
          preStmt.clear();
        }
  
      } else {
        /*
         * regular line
         */
        while ( pos < line.size() ) {
          if ( inComment ) {
            // look for comment end
            if ( line[pos] == '*' && line[pos+1] == '/' ) {
              ctext += "*/";
              inComment = false;
              pos += 2;
	      if ( gen_comments ) {
	        pdbComment* com = file->addComment(comment_id++);
	        com->kind(PDB::LA_C);
	        com->cmtBegin(pdbLoc(file, cli, cco));
	        com->cmtEnd(pdbLoc(file, lineno, pos));
	        com->text(ctext);
	      }
              ctext = "";
            } else {
              ctext += line[pos++];
            }

          } else if ( line[pos] == '/' ) {
            pos++;
            if ( line[pos] == '/' ) {
              // c++ comments
              pos++;
              cli = lineno;
              cco = pos-1;
              ctext += "//";
              while ( pos < line.size() ) { ctext += line[pos++]; }
	      if ( gen_comments ) {
	        pdbComment* com = file->addComment(comment_id++);
	        com->kind(PDB::LA_CXX);
	        com->cmtBegin(pdbLoc(file, cli, cco));
	        com->cmtEnd(pdbLoc(file, lineno, pos));
	        com->text(ctext);
	      }
              ctext = "";
            } else if ( line[pos] == '*' ) {
              // c comment start
              pos++;
              ctext += "/*";
              inComment = true;
              cli = lineno;
              cco = pos-1;
            }

          } else if ( line[pos] == '\"' ) {
            // character string constant
            do {
              pos++;
              if ( line[pos] == '\\' ) {
                pos++;
                if ( line[pos] == '\"' ) { pos++; }
              }
            }
            while ( line[pos] != '\"' && pos < line.size());
            pos++;

          } else if ( line[pos] == '\'' ) {
            // character constant
            do {
              pos++;
              if ( line[pos] == '\\' ) {
                pos++;
                if ( line[pos] == '\'' ) { pos++; }
              }
            }
            while ( line[pos] != '\'' && pos < line.size() );
            pos++;
          } else {
            pos++;
          }
        }
      }
      if ( inComment ) ctext += "\\n";

      // CLL START
      if (!inCll) {
        line_cll_c++;
        os_cll << line << endl;
      }
      else {
      	inCll = false;
      }
      // CLL END

      // Initialize cll at main function
      if (lineno == pos_main) {
        line_cll_c++;
        line_cll_c++;
        os_cll << "#line " << line_cll_c << " \"" << fn << "\"" << endl; // #line XX
        os_cll << "\tint cll_INIT = cll_Init();" << endl;
        line_cll_c++;
        os_cll << "#line " << lineno + 1 << " \"" << name << "\"" << endl;
      }


      ++lineno;
      pos = 0;
    }

   // Generate cll.h    
   generate_cll_h (name, fn_h,  os_cllh, vec_exp, vec_uses);    
 
  }


  void find_f_comments_in(pdbFile* file, PDB& pdb, std::vector<string>& vec_opt ) {
    const string& name = file->name();
    string line;
    int lineno = 0;
    bool inCll = false;
    int cli = 0, cco = 0, cstart = 0, cend = 0;
    bool inString1 = false, inString2 = false;
    vector<string> cStmt;
    string currComPrefix, lastComPrefix;

    ifstream is(name.c_str());
    if ( !is ) {
      cerr << "ERROR: cannot open " << name << endl;
      return;
    }


    // CLL START
    std::string fn = name.c_str();
    fn.replace(fn.find_last_of("."), 1, ".cll.");
    ofstream os_cllf(fn.c_str());
    if ( !os_cllf ) {
      cerr << "ERROR: cannot open " << fn << endl;
      return;
    }
 
    std::string fn_h = name.c_str();
    fn_h.replace(fn_h.find_last_of("."), 2, ".cll.fi");
    ofstream os_cllfh(fn_h.c_str());
    if ( !os_cllfh ) {
      cerr << "ERROR: cannot open " << fn_h << endl;
      return;
    }
 



    int pos_main = 0;
    for (PDB::froutinevec::iterator r = pdb.getFRoutineVec().begin(); r!=pdb.getFRoutineVec().end(); r++) {
      if ((*r)->name() == "MAIN") {
        pos_main = (*r)->location().line();
        break;
      }
    } 




   
    // CLL END

    comment_id = 0;

    while ( getline(is, line) ) {
      ++lineno;
      string lowline(line);
      transform(line.begin(), line.end(), lowline.begin(), fo_tolower());

      if ( line.size() &&
           (lowline[0] == 'c' || lowline[0] == '*' ||
	    lowline[0] == dComChar ) ) {
	/*
         * fix form comment
         */
	currComPrefix = get_fcom_prefix(lowline);
	if ( cStmt.empty() ) {
	  // new comment block
          cli = lineno;
          cco = 1;
	  lastComPrefix = currComPrefix;
	} else if ( lastComPrefix != currComPrefix ) {
	  // new comment block because different prefix
	  process_fcom(cStmt, cli, cco, cend, file, pdb, lastComPrefix, os_cllf, inCll, vec_opt);
          cli = lineno;
          cco = cstart+1;
	  lastComPrefix = currComPrefix;
	} else if ( currComPrefix.size() > 1 &&
	            (line[5] == ' ' || line[5] == '0') ) {
	  // new comment block because start of new directive
	  // (no directive continuation line)
	  process_fcom(cStmt, cli, cco, cend, file, pdb, lastComPrefix, os_cllf, inCll, vec_opt);
          cli = lineno;
          cco = 1;
	}
	cStmt.push_back(line);
	cend = line.size();
      } else if ( line.size() &&
        (cstart = lowline.find_first_not_of(" \t")) != string::npos &&
	lowline[cstart] == '!') {
	/*
         * free form full line comment
         */
	currComPrefix = get_fcom_prefix(lowline.substr(cstart));
	if ( cStmt.empty() ) {
	  // new comment block
          cli = lineno;
          cco = cstart+1;
	  lastComPrefix = currComPrefix;
	} else if ( lastComPrefix != currComPrefix ) {
	  // new comment block because different prefix
	  process_fcom(cStmt, cli, cco, cend, file, pdb, lastComPrefix, os_cllf, inCll, vec_opt);
          cli = lineno;
          cco = cstart+1;
	  lastComPrefix = currComPrefix;
	}
	
	cStmt.push_back(line);
	cend = line.size();

	if ( currComPrefix.size() > 1 && has_no_cont(line, cstart+1) ) {
	  // end of pragma (no directive continuation line)
	  process_fcom(cStmt, cli, cco, cend, file, pdb, lastComPrefix, os_cllf, inCll, vec_opt);
	}
      } else {
        /*
         * normal line
         */
	// commit comment block if not done already
	if ( cStmt.size() ) {
	  process_fcom(cStmt, cli, cco, cend, file, pdb, lastComPrefix, os_cllf, inCll, vec_opt);
        }

	// scan line for inline comments ignoring character constants
	int pos=0;
        while ( pos < line.size() ) {
	  if ( inString1 ) {
	    if ( line[pos] == '\'' ) {
              pos++;
	      // ' is actually continuation character?
	      if ( pos == 6 && line.find_first_not_of(" \t") == 5 ) continue;
	      // only single ' does end string
	      if ( line[pos] != '\'' ) {
		inString1 = false;
              }
	    }
          } else if ( inString2 ) {
	    if ( line[pos] == '\"' ) {
              pos++;
	      // " is actually continuation character?
	      if ( pos == 6 && line.find_first_not_of(" \t") == 5 ) continue;
	      // only single " does end string
	      if ( line[pos] != '\"' ) {
		inString2 = false;
              }
	    }
	  } else if ( line[pos] == '\'' ) {
	    inString1 = true;
	  } else if ( line[pos] == '\"' ) {
	    inString2 = true;
	  } else if ( line[pos] == '!' ) {
	    if ( gen_comments ) {
	      pdbComment* com = file->addComment(comment_id++);
	      com->kind(PDB::LA_FORTRAN);
	      com->cmtBegin(pdbLoc(file, lineno, pos+1));
	      com->cmtEnd(pdbLoc(file, lineno, line.size()));
	      com->text(line.substr(pos));
	    }
	    break;
	  }
	  pos++;
	}
      }
      
      // CLL START
      if (!inCll) {
        line_cll_c++;
        os_cllf << line << endl;
      }
      else {
      	inCll = false;
      }
      // CLL END
     

      // Initialize cll at main function
      if (lineno == pos_main) {
        line_cll_c++;
        os_cllf << "!line " << line_cll_c << " \"" << fn << "\"" << endl; // #line XX
        line_cll_c += 2;
        os_cllf << endl;
        os_cllf << "      include '" << fn_h << "'" << endl;
        line_cll_c++;
        os_cllf << "      integer cll_INIT = cll_Init()" << endl;
        os_cllf << "!line " << lineno  << " \"" << name << "\"" << endl;
      } 
      
    }

    //generate_cll_f (os_cllfh);
    generate_cll_f (name, fn_h,  os_cllfh, vec_exp, vec_uses);    
    
    // commit comment block at end of file
    if ( cStmt.size() ) {
      process_fcom(cStmt, cli, cco, cend, file, pdb, lastComPrefix, os_cllf, inCll, vec_opt);
    }
  }
}



//---------------------------------------------------------------------------

int main(int argc, char *argv[]) {
  int ch;
  bool errflag = argc < 2;
  bool verbose = false;
  bool screen = false;
  char *outfile = 0;
  // Indicate each directive/pragma to use in fortran
  directives.push_back("$omp");
  directives.push_back("$cll");

  while ( (ch = getopt(argc, argv, "cdo:spvD:")) != -1 ) {
    switch (ch) {
    case 'c': gen_comments = true;
	      gen_pragmas  = false;
	      break;
    case 'p': gen_pragmas  = true;
	      gen_comments = false;
	      break;
    case 'd': dComChar = 'd';
	      break;
    case 'v': verbose = true;
	      break;
    case 'o': outfile = optarg;
	      break;
    case 's': screen = true;
	      break;
    case 'D': {
		string dir = optarg;
                transform(dir.begin(), dir.end(), dir.begin(), fo_tolower());
		directives.push_back(dir);
	      }
	      break;
    case '?': errflag = true;
	      break;
    }
  }
  if ( errflag ) {
    cerr << "usage: " << argv[0]
         << " [-o outfile|s] [-v] [-c|-p] [-d] [-D directive] pdbfile" << "\n\n";

    cerr << "Scans all (non-system) source files related to a PDB file \n"
      "for C, C++, Fortran comments, C/C++ pragmas, and Fortran\n"
      "directives and prints out a new enhanced PDB file containing\n"
      "this additional information.\n"
      "\n"
      "Options:\n\n"
      "     -o <outfile>  Write output to file <outfile>\n\n"
      "     -s            Write output to screen\n\n"
      "     -c            Only scan for comments (ignore pragmas)\n\n"
      "     -p            Only scan for pragmas (ignore comments)\n\n"
      "     -d            Fortran only: Consider lines with a 'D'\n"
      "                   in the first column also as comments\n\n"
      "     -v            Verbose mode\n\n"
      "     -D string     Fortran only: Scan also for directives\n"
      "                   which are marked with the sentinel 'string'.\n"
      "                   This option can be specified several times,\n"
      "                   once for each directive sentinel to scan for.\n"
      "                   Case does NOT matter when specifiying 'string'.\n"
      "                   pdbcomment recognizes OpenMP (sentinel $omp)\n"
      "                   by default.\n\n";





    return 1;
  }



  // open and initialize program database
  PDB pdb(argv[optind]);
  if ( !pdb ) return 1;

  // build file list
  PDB::filevec& files = pdb.getFileVec();


  vector<string> vec_opt;
  for(PDB::filevec::iterator fi=files.begin(); fi!=files.end(); ++fi) {
    if ( ! (*fi)->isSystemFile() ) {
      if ( verbose ) cout << "Processing " << (*fi)->name() << " ..." << endl;
      if ( pdb.language() & PDB::LA_C_or_CXX ) {
         // Ignore files from includes at compiling time
         if ((*fi)->name().find("/usr/lib/") == std::string::npos) {
            find_c_comments_in(*fi, pdb, vec_opt);
         }
      } else {
         // Ignore files from includes (mpif) at compiling time
         if ((*fi)->name().find("mpif") == std::string::npos) {
            find_f_comments_in(*fi, pdb, vec_opt);
         }
      }
    } else {
      if ( verbose ) cout << "Ignoring " << (*fi)->name() << " ..." << endl;
    }
  }

  // commit pragma items
  if ( gen_pragmas ) pdb.finalCheck(PDB::pragmaTag());

  // preare output
  if ( outfile ) {
    if ( verbose ) cout << "Writing " << outfile << " ..." << endl;
    pdb.write(outfile);
  } else if (screen) {
    if ( verbose ) cout << "Writing on screen" << " ..." << endl;
    pdb.write(cout);
  }

  return 0;
}


